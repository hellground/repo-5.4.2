#include "GameObjectAI.h"
#include "ObjectMgr.h"
#include "ScriptMgr.h"
#include "ScriptedCreature.h"
#include "SpellScript.h"
#include "SpellAuraEffects.h"
#include "SpellAuras.h"
#include "MapManager.h"
#include "Spell.h"
#include "Vehicle.h"
#include "Cell.h"
#include "CellImpl.h"
#include "GridNotifiers.h"
#include "GridNotifiersImpl.h"
#include "CreatureTextMgr.h"
#include "Unit.h"
#include "Player.h"
#include "Creature.h"
#include "InstanceScript.h"
#include "Map.h"
#include "VehicleDefines.h"
#include "SpellInfo.h"

#include "throne_of_thunder.h"

enum eJikunEvents
{
    // Ji kun
    EVENT_GOTO_FIRST_POS       = 1,
    EVENT_GOTO_CENTER          = 2,
    EVENT_END_MOVE             = 3,
    EVENT_TALON_RAKE           = 4,
    EVENT_QUILS                = 5,
    EVENT_DOWN_DRAFT           = 6,
    EVENT_CAW                  = 7,
    EVENT_ACTIVATE_NEST        = 8,
    EVENT_FEED_YOUNG           = 9,
}; 

enum eJikunSpells
{
    // Infected talons
    SPELL_AURA_TALO_PROC                = 140094,

    // Talon Rake
    SPELL_TALON_RAKE                    = 134366,

    // Quils
    SPELL_QUILS                         = 134380,

    // Down Draft
    SPELL_DOWN_DRAFT                    = 134370,

    // Caw
    SPELL_CAW                           = 138923,

    // feed young
    SPELL_FEED                          = 137528,

    // feed pool
    SPELL_FEED_POOL_DMG                 = 138319,
    SPELL_FEED_POOL_VISIBLE             = 138854,
    SPELL_FEED_POOL_VISIBLE_HATCHLING   = 139284,
    SPELL_SUMMON_POOL                   = 134259, // main platform
    SPELL_SUMMON_POOL_HATCHLINGS        = 139285, // other

    // drop feather
    SPELL_DROP_FEATHER                  = 134338, // 140016

    // flying
    SPELL_FLYING                        = 134339,

    // Cheep aoe
    SPELL_CHEEP_AOE                     = 140129,
};

Position IntroMoving[2] =
{
    { 6213.971f,  4289.072f, -14.402f, 2.873f },
    { 6146.085f,  4319.261f, -30.608f, 2.739f }
};

Position NestPositionsGround[5] =
{
    { 6071.182f,  4285.108f, -101.469f, 1.873f },
    { 6096.028f,  4339.460f, -93.655f, 1.873f },
    { 6159.814f,  4370.529f, -70.502f, 1.873f },
    { 6220.071f,  4333.520f, -59.075f, 1.873f },
    { 6192.708f,  4267.664f, -70.764f, 1.873f }
};

Position NestPositionsHigh[5] =
{
    { 6078.422f,  4270.403f, 42.407f, 1.873f },
    { 6082.500f,  4371.428f, 45.238f, 1.873f },
    { 6151.905f,  4330.750f, 72.997f, 1.873f },
    { 6217.987f,  4352.961f, 68.138f, 1.873f },
    { 6173.894f,  4239.375f, 43.848f, 1.873f }
};

class boss_jikun : public CreatureScript
{
    public:
        boss_jikun() : CreatureScript("boss_jikun") { }

        struct boss_jikunAI : public BossAI
        {
            boss_jikunAI(Creature* creature) : BossAI(creature, DATA_JI_KUN)
            {
                instance = creature->GetInstanceScript();
                down_draft_time = 500;
                lastNestActivated = 5;
                firstNest = true;
                lastNestHigh = false;
                SummonEggs();
            }

            bool firstNest,lastNestHigh;
            uint32 lastNestActivated;
            InstanceScript* instance;
            EventMap events;
            uint32 down_draft_time;

            void SummonEggs()
            {
                for (uint8 i = 0; i<=4;++i)
                {
                    for (uint8 j = 0;j<=15;++j)
                    {
                        Position summonPos;
                        me->GetRandomPoint(NestPositionsGround[i],7.0f,summonPos);
                        me->SummonCreature(68194,summonPos.GetPositionX(),summonPos.GetPositionY(),summonPos.GetPositionZ());
                        if (TempSummon * egg = me->SummonCreature(68194,summonPos.GetPositionX(),summonPos.GetPositionY(),summonPos.GetPositionZ()))
                            egg->SetFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_NON_ATTACKABLE | UNIT_FLAG_NOT_SELECTABLE);

                    }
                    for (uint8 c = 0;c<=13;++c)
                    {
                        Position summonPosHigh;
                        me->GetRandomPoint(NestPositionsHigh[i],i == 3 ? 3.0f : 4.0f,summonPosHigh);

                        if (TempSummon * egg = me->SummonCreature(69628,summonPosHigh.GetPositionX(),summonPosHigh.GetPositionY(),summonPosHigh.GetPositionZ()))
                            egg->SetFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_NON_ATTACKABLE | UNIT_FLAG_NOT_SELECTABLE);
                    }
                }
            }

            void DespawnCreature(uint32 entry)
            {
                std::list<Creature*> creatureList;
                GetCreatureListWithEntryInGrid(creatureList, me, entry, 700.0f);
                for (auto NowCreature : creatureList)
                    NowCreature->DespawnOrUnsummon();
            }
            void RemoveAllAura()
            {
                std::list<Player*> PlayerList;
                GetPlayerListInGrid(PlayerList, me, 1000.0f);
                for (auto player : PlayerList)
                {
                    if (player->HasAura(140741))
                        player->RemoveAura(140741);
                    if (player->HasAura(SPELL_TALON_RAKE))
                        player->RemoveAura(SPELL_TALON_RAKE);
                    if (player->HasAura(134339))
                        player->RemoveAura(134339);
                    if (player->HasAura(138309))
                        player->RemoveAura(138309);
                }
            }
            void Reset()
            {
                RemoveAllAura();
                DespawnCreature(70216);
                DespawnCreature(68188);
                DespawnCreature(70095);
                me->SetDisableGravity(true);
                me->SetCanFly(true);
                firstNest = true;
                lastNestHigh = false;

                _Reset();
                events.Reset();
                summons.DespawnAll();
                if (instance)
                {
                    instance->SendEncounterUnit(ENCOUNTER_FRAME_DISENGAGE, me);
                    instance->SetData(DATA_JI_KUN, NOT_STARTED);
                    if (instance->GetData(DATA_JI_KUN_EVENT) != DONE)
                    {
                        me->SetVisible(false);
                        me->SetFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_NON_ATTACKABLE);
                        me->SetReactState(REACT_PASSIVE);
                        if (Creature* egg = instance->instance->GetCreature(instance->GetData64(NPC_EGG_SUMMON)))
                            egg->Respawn();
                    }
                    else
                    {
                        SummonEggs();
                        me->SetHomePosition(IntroMoving[1]);
                        me->GetMotionMaster()->MoveTargetedHome();
                    }
                }
            }

            void EnterCombat(Unit* attacker)
            {
                if (instance)
                {
                    instance->SetBossState(DATA_JI_KUN, IN_PROGRESS);
                    instance->SendEncounterUnit(ENCOUNTER_FRAME_ENGAGE, me);
                }

                events.ScheduleEvent(EVENT_TALON_RAKE, 5000); 
                events.ScheduleEvent(EVENT_QUILS, 40000);
                events.ScheduleEvent(EVENT_DOWN_DRAFT, 90000);
                events.ScheduleEvent(EVENT_CAW, 16000);
                events.ScheduleEvent(EVENT_ACTIVATE_NEST, 12000);
                
                DoCast(SPELL_AURA_TALO_PROC);
            }

            void JustReachedHome()
            {
                _JustReachedHome();
 
                if (instance)
                    instance->SetBossState(DATA_JI_KUN, FAIL);
            }

            void SummonedCreatureDespawn(Creature* summon)
            {
                summons.Despawn(summon);
            }

            void JustSummoned(Creature* summon)
            {
                summons.Summon(summon);
				if (me->isInCombat())
					summon->SetInCombatWithZone();

                if (summon->GetEntry() == 68192)
                {
                    summon->setFaction(me->getFaction());
                    std::list<Creature*> EggList;
                    GetCreatureListWithEntryInGrid(EggList, summon, 68194, 10.0f);
                    if (!EggList.empty())
                        if (Creature *target = JadeCore::Containers::SelectRandomContainerElement(EggList))
                            target->DespawnOrUnsummon();
                       
                }
            }

            void KilledUnit(Unit* who)
            {
            }

            void JustDied(Unit* killer)
            {
                DespawnCreature(70216);
                DespawnCreature(68188);
                DespawnCreature(70095);
                RemoveAllAura();
                instance->SendEncounterUnit(ENCOUNTER_FRAME_DISENGAGE, me);
                instance->SetData(DATA_JI_KUN, DONE);
                _JustDied();
            }

            void DoAction(const int32 action)
            {
                if (action == 1) // intro done
                {
                    events.ScheduleEvent(EVENT_GOTO_FIRST_POS, 100);
                    events.ScheduleEvent(EVENT_GOTO_CENTER, 7000);
                    events.ScheduleEvent(EVENT_END_MOVE, 16000);
                    me->SetFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_NON_ATTACKABLE);
                    me->SetVisible(true);
                    me->SetHomePosition(IntroMoving[1]);
                    DoPlaySoundToSet(me, 36213); // pre agro
                }

            }

            void UpdateAI(const uint32 diff)
            {
                if (instance->GetData(DATA_JI_KUN_EVENT) != DONE)
                {
                    events.Update(diff);

                    switch (events.ExecuteEvent())
                    {
                    case EVENT_GOTO_FIRST_POS:
                    {
                        me->GetMotionMaster()->MovePoint(0, IntroMoving[0]);
                        break;
                    }
                    case EVENT_GOTO_CENTER:
                    {
                        events.CancelEvent(EVENT_GOTO_FIRST_POS);
                        me->GetMotionMaster()->MovePoint(0, IntroMoving[1]);
                        break;
                    }
                    case EVENT_END_MOVE:
                    {
                        events.CancelEvent(EVENT_GOTO_CENTER);
                        me->RemoveFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_NON_ATTACKABLE);
                        me->SetReactState(REACT_AGGRESSIVE);
                        instance->SetData(DATA_JI_KUN_EVENT, DONE);
                        break;
                    }
                    default:
                        break;
                    }
                }
                else
                {
                    if (!UpdateVictim())
                        return;

                    if (me->HasAura(SPELL_DOWN_DRAFT))
                    {
                        if (down_draft_time < diff)
                        {
                            std::list<Player*> PlayerList;
                            GetPlayerListInGrid(PlayerList, me, 50.0f);
                            for (auto playerTarget : PlayerList)
                                if (!playerTarget->HasAura(SPELL_FLYING))
                                    playerTarget->NearTeleportTo(playerTarget->GetPositionX() - 3.0f * cos(playerTarget->GetAngle(me)),playerTarget->GetPositionY() - 1.2f * sin(playerTarget->GetAngle(me)),playerTarget->GetPositionZ(),playerTarget->GetOrientation());

                            down_draft_time = 500;
                        }
                        else down_draft_time -= diff;
                    }

                    events.Update(diff);

                    switch (events.ExecuteEvent())
                    {
                    case EVENT_ACTIVATE_NEST:
                    {
                        bool IsHighActivate = urand(0,1);
                        uint8 posNum = urand(0,4);
                        while (posNum == lastNestActivated)
                            posNum = urand(0,4);
                        if (firstNest)
                        {
                            IsHighActivate = false;
                            posNum = 0;
                        }
                        if (lastNestHigh)
                            IsHighActivate = false;
                        uint8 k = IsHighActivate ? 2 : 3;
                        for (uint8 i = 0;i<=k;++i)
                        {
                            Position summonPos;
                            me->GetRandomPoint(IsHighActivate ? NestPositionsHigh[posNum] : NestPositionsGround[posNum],6.0f,summonPos);

                            if (TempSummon * egg = me->SummonCreature(IsHighActivate ? 69628 : 68192,summonPos.GetPositionX(),summonPos.GetPositionY(),summonPos.GetPositionZ()))
                                if (IsHighActivate)
                                {
                                    egg->RemoveFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_NON_ATTACKABLE | UNIT_FLAG_NOT_SELECTABLE);
                                    egg->setFaction(me->getFaction());
                                    egg->SetReactState(REACT_PASSIVE);
                                    std::list<Creature*> EggList;
                                    GetCreatureListWithEntryInGrid(EggList, egg, 69628, 7.0f);
                                    if (!EggList.empty())
                                        if (Creature *target = JadeCore::Containers::SelectRandomContainerElement(EggList))
                                               target->DespawnOrUnsummon();
                                }
                        }

                        if (IsHighActivate)
                            lastNestHigh = true;

                        me->SummonCreature(68208, IsHighActivate ? NestPositionsHigh[posNum].GetPositionX() : NestPositionsGround[posNum].GetPositionX(),IsHighActivate ? NestPositionsHigh[posNum].GetPositionY() : NestPositionsGround[posNum].GetPositionY(),IsHighActivate ? -31.00f : NestPositionsGround[posNum].GetPositionZ(),0.0f,TEMPSUMMON_TIMED_DESPAWN,20000);

                        lastNestActivated = posNum;
                        firstNest = false;
                        events.ScheduleEvent(EVENT_FEED_YOUNG, 10000);
                        events.ScheduleEvent(EVENT_ACTIVATE_NEST, 30000);
                        break;
                    }
                    case EVENT_FEED_YOUNG:
                    {
                        me->StopMoving();
                        DoCast(SPELL_FEED);
                        break;
                    }
                    case EVENT_TALON_RAKE:
                    {
                        me->StopMoving();
                        if (!me->HasUnitState(UNIT_STATE_CASTING) && me->getVictim())
                        {
                            DoCast(me->getVictim(),SPELL_TALON_RAKE);
                            events.ScheduleEvent(EVENT_TALON_RAKE, 20000);
                        }
                        else
                            events.ScheduleEvent(EVENT_TALON_RAKE, 2000);
                        break;
                    }
                    case EVENT_QUILS:
                    {
                        me->StopMoving();
                        DoCast(SPELL_QUILS);
                        events.ScheduleEvent(EVENT_QUILS, 60000);
                        break;
                    }
                    case EVENT_DOWN_DRAFT:
                    {
                        me->StopMoving();
                        down_draft_time = 500;
                        DoCast(SPELL_DOWN_DRAFT);
                        events.ScheduleEvent(EVENT_DOWN_DRAFT, 90000);
                        break;
                    }
                    case EVENT_CAW:
                    {
                        me->StopMoving();
                        if (!me->HasUnitState(UNIT_STATE_CASTING))
                        {
                            DoCast(SPELL_CAW);
                            events.ScheduleEvent(EVENT_CAW, 16000);
                        }
                        else
                            events.ScheduleEvent(EVENT_CAW, 2000);
                        break;
                    }
                    default:
                        break;
                    }

                    if (me->HasUnitState(UNIT_STATE_CASTING))
                        return;

                    DoMeleeAttackIfReady();
                }
            }
        };

        CreatureAI* GetAI(Creature* creature) const
        {
            return new boss_jikunAI(creature);
        }
};

// beam target - 68208
class jikun_beam_target : public CreatureScript
{
    public:
        jikun_beam_target() : CreatureScript("jikun_beam_target") { }

        struct jikun_beam_targetAI : public ScriptedAI
        {
            jikun_beam_targetAI(Creature* creature) : ScriptedAI(creature) 
            { 
                me->SetHomePosition(me->GetPositionX(),me->GetPositionY(),me->GetPositionZ(),me->GetOrientation());
                me->SetDisableGravity(true);
                me->SetCanFly(true);
                me->SetDisplayId(11686);
                me->setFaction(14);
                me->SetReactState(REACT_PASSIVE);
                me->SetFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_NON_ATTACKABLE | UNIT_FLAG_NOT_SELECTABLE);
                me->SetObjectScale(5.0f);
                me->AddAura(96869,me);
            }
        };

        CreatureAI* GetAI(Creature* creature) const
        {
            return new jikun_beam_targetAI(creature);
        }
};

// young hatchling - 68192
class young_hatchling_jikun : public CreatureScript
{
    public:
        young_hatchling_jikun() : CreatureScript("young_hatchling_jikun") { }

        struct young_hatchling_jikunAI : public ScriptedAI
        {
            young_hatchling_jikunAI(Creature* creature) : ScriptedAI(creature) { }

            uint32 timeCheep,timeCheckEat,timEggSummon;
            void Reset()
            {
                me->SetHomePosition(me->GetPositionX(),me->GetPositionY(),me->GetPositionZ(),me->GetOrientation());
                me->SetDisableGravity(true);
                me->SetCanFly(true);
                timeCheep = urand(4000,8000);
                timeCheckEat = 3000;
                timEggSummon = 8000;
            }

            void UpdateAI(const uint32 diff)
            { 
                if (me->GetDistance(me->GetHomePosition()) > 9.0f)
                {
                    me->CombatStop(true);
                    me->GetMotionMaster()->MoveTargetedHome();
                }

                if (!me->HasAura(134322))
                {
                    if (timeCheckEat < diff)
                    {
                        std::list<Creature*> creatureList;
                        GetCreatureListWithEntryInGrid(creatureList, me, 70216, 16.0f);

                        if (!creatureList.empty())
                            if (Creature *pool = JadeCore::Containers::SelectRandomContainerElement(creatureList))
                            {
                                me->SetReactState(REACT_PASSIVE);
                                me->GetMotionMaster()->MovePoint(0,pool->GetPositionX(),pool->GetPositionY(),pool->GetPositionZ());
                                timeCheckEat = 5000;
                            }
                            else
                                timeCheckEat = 5000;
                    }
                    else timeCheckEat -= diff;
                }

                if (me->GetReactState() == REACT_PASSIVE)
                {
                    std::list<Creature*> creatureList;
                    GetCreatureListWithEntryInGrid(creatureList, me, 70216, 1.0f);
                    if (!creatureList.empty())
                        if (Creature *pool = JadeCore::Containers::SelectRandomContainerElement(creatureList))
                        {
                            DoCast(134321);
                            pool->DespawnOrUnsummon();
                        }
                }

                if (me->HasAura(134322) && me->GetReactState() == REACT_PASSIVE)
                    me->SetReactState(REACT_AGGRESSIVE);

                if (me->GetReactState() != REACT_PASSIVE)
                {
                    if (timeCheep < diff)
                    {
                        if (me->getVictim())
                            DoCast(me->getVictim(),me->HasAura(134322) ? 140570 : 139296);
                        timeCheep = urand(5000,15000);
                    }
                    else timeCheep -= diff;
                }

                if (me->GetReactState() == REACT_AGGRESSIVE && me->HasAura(134322))
                {
                    if (timEggSummon < diff)
                    {
                        if (TempSummon * egg = me->SummonCreature(69628,me->GetPositionX(),me->GetPositionY(),me->GetPositionZ()))
                        {
                            egg->RemoveFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_NON_ATTACKABLE | UNIT_FLAG_NOT_SELECTABLE);
                            egg->setFaction(me->getFaction());
                            egg->SetReactState(REACT_PASSIVE);
                        }
                        timEggSummon = 50000;
                    }
                    else timEggSummon -= diff;
                }


                if (!me->HasUnitState(UNIT_STATE_CASTING))
                    DoMeleeAttackIfReady();
            }

            void JustDied(Unit* killer)
            {
                std::list<Player*> PlayerList;
                GetPlayerListInGrid(PlayerList, me, 10.0f);
                for (auto playerTarget : PlayerList)
                    if (!playerTarget->HasAura(140571))
                    {
                        playerTarget->AddAura(134339,playerTarget);
                        playerTarget->AddAura(140571,playerTarget);
                        if (playerTarget->HasAura(134339))
                            playerTarget->GetAura(134339)->SetStackAmount(4);
                        break;
                    }
            }
        };

        CreatureAI* GetAI(Creature* creature) const
        {
            return new young_hatchling_jikunAI(creature);
        }
};

// Juvenile - 70095
class npc_juvenile : public CreatureScript
{
    public:
        npc_juvenile() : CreatureScript("npc_juvenile") { }

        struct npc_juvenileAI : public ScriptedAI
        {
            npc_juvenileAI(Creature* creature) : ScriptedAI(creature) 
            {
                isCenter = false;
            }

            uint32 timeCheep;
            bool isCenter;
            void Reset()
            {
                me->SetHomePosition(me->GetPositionX(),me->GetPositionY(),me->GetPositionZ(),me->GetOrientation());
                me->SetDisableGravity(true);
                me->SetCanFly(true);
                timeCheep = 7000;
            }

            void UpdateAI(const uint32 diff)
            { 
                if (!isCenter)
                {
                    me->SetReactState(REACT_PASSIVE);
                    Position center;
                    me->GetRandomPoint(IntroMoving[1], 25.0f, center);
                    me->GetMotionMaster()->MovePoint(0, center.GetPositionX(), center.GetPositionY(), 28.0f);
                    isCenter = true;
                }
                else
                {
                    if (timeCheep < diff)
                    {
                        std::list<Player*> PlayerList;
                        GetPlayerListInGrid(PlayerList, me, 150.0f);
                        for (auto playerTarget : PlayerList)
                            if (playerTarget->HasAura(134339))
                                PlayerList.remove(playerTarget);

                        if (!PlayerList.empty())
                            if (Player *target = JadeCore::Containers::SelectRandomContainerElement(PlayerList))
                                if (target)
                                    DoCast(target,SPELL_CHEEP_AOE);
                        timeCheep = urand(5000,15000);
                    }
                    else timeCheep -= diff;

                    if (!me->HasUnitState(UNIT_STATE_CASTING))
                        DoMeleeAttackIfReady();
                }
            }
        };

        CreatureAI* GetAI(Creature* creature) const
        {
            return new npc_juvenileAI(creature);
        }
};


// Mature Egg of Ji-Kun - 69628
class egg_mature_jikun : public CreatureScript
{
    public:
        egg_mature_jikun() : CreatureScript("egg_mature_jikun") { }

        struct egg_mature_jikunAI : public ScriptedAI
        {
            egg_mature_jikunAI(Creature* creature) : ScriptedAI(creature) { }

            uint32 timeToSummonHatchling;

            void Reset()
            {
                timeToSummonHatchling = 25000;
            }

            void UpdateAI(const uint32 diff)
            { 
                if (me->HasFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_NOT_SELECTABLE))
                    return;

                if (timeToSummonHatchling < diff)
                {
                    if (me->ToTempSummon())
                    if (Unit * summoner = me->ToTempSummon()->GetSummoner())
                        if (TempSummon * hatchling = summoner->SummonCreature(70095,me->GetPositionX(),me->GetPositionY(),me->GetPositionZ()))
                        {
                            hatchling->SetMaxHealth(me->GetMaxHealth());
                            hatchling->SetHealth(me->GetHealth());
                            hatchling->setFaction(summoner->getFaction());
                        }
                    me->DespawnOrUnsummon();
                    timeToSummonHatchling = 25000;
                }
                else timeToSummonHatchling -= diff;
            }
        };

        CreatureAI* GetAI(Creature* creature) const
        {
            return new egg_mature_jikunAI(creature);
        }
};

// Caw 138923
class spell_caw : public SpellScriptLoader
{
    public:
        spell_caw() : SpellScriptLoader("spell_caw") { }

        class spell_caw_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_caw_SpellScript);

            void SelectTarget(std::list<WorldObject*>& targets)
            {
                if (!GetCaster())
                    return;
                std::list<Player*> PlayerList;
                GetPlayerListInGrid(PlayerList, GetCaster(), 50.0f);
                if (!PlayerList.empty())
                    if (Player *target = JadeCore::Containers::SelectRandomContainerElement(PlayerList))
                        GetCaster()->CastSpell(target,138926,true);
            }

            void Register()
            {
                OnObjectAreaTargetSelect += SpellObjectAreaTargetSelectFn(spell_caw_SpellScript::SelectTarget, EFFECT_0, TARGET_UNIT_SRC_AREA_ENEMY);
            }
        };

        SpellScript* GetSpellScript() const
        {
            return new spell_caw_SpellScript();
        }
};

// Infected talons proc 140094
class spell_infected_talons : public SpellScriptLoader
{
    public:
        spell_infected_talons() : SpellScriptLoader("spell_infected_talons") { }

        class spell_infected_talons_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_infected_talons_AuraScript);

            void OnProc(constAuraEffectPtr aurEff, ProcEventInfo& eventInfo)
            {
                PreventDefaultAction();

                if (!GetCaster() || !GetCaster()->getVictim())
                    return;

                GetCaster()->CastSpell(GetCaster()->getVictim(),140092,true);
            }

            void Register()
            {
                OnEffectProc += AuraEffectProcFn(spell_infected_talons_AuraScript::OnProc, EFFECT_0, SPELL_AURA_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const
        {
            return new spell_infected_talons_AuraScript();
        }
};

// hatchling eated 134321
class spell_hatchling_eated : public SpellScriptLoader
{
    public:
        spell_hatchling_eated() : SpellScriptLoader("spell_hatchling_eated") { }

        class spell_hatchling_eated_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_hatchling_eated_AuraScript);

            void OnRemove(constAuraEffectPtr /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                if (GetTarget())
                    GetTarget()->CastSpell(GetTarget(),134322,true);
            }

            void Register()
            {
                AfterEffectRemove += AuraEffectRemoveFn(spell_hatchling_eated_AuraScript::OnRemove, EFFECT_0, SPELL_AURA_DUMMY, AURA_EFFECT_HANDLE_REAL);

            }
        };

        AuraScript* GetAuraScript() const
        {
            return new spell_hatchling_eated_AuraScript();
        }
};

// 138319
class pool_of_feed_dmg_aura : public SpellScriptLoader
{
    public:
        pool_of_feed_dmg_aura() : SpellScriptLoader("pool_of_feed_dmg_aura") { }

        class pool_of_feed_dmg_aura_AuraScript : public AuraScript
        {
            PrepareAuraScript(pool_of_feed_dmg_aura_AuraScript);

            void OnPeriodic(constAuraEffectPtr aurEff)
            {
                if (!GetTarget() || !aurEff)
                    return;

                if (GetId() == 134256)
                    return;

                if (!GetCaster() && GetTarget()->HasAura(138319))
                {
                    GetTarget()->RemoveAura(138319);
                    return;
                }

                if (!GetCaster())
                    return;

                if (aurEff->GetTickNumber() == 3)
                    if (TempSummon * pool = GetCaster()->ToTempSummon())
                    {
                        if (GetTarget()->HasAura(138319))
                            GetTarget()->RemoveAura(138319);
                        pool->DespawnOrUnsummon();
                        return;
                    }

                if (GetTarget()->GetDistance(GetCaster()) > 4.0f)
                    if (GetTarget()->HasAura(138319))
                        GetTarget()->RemoveAura(138319);
            }

            void OnRemove(constAuraEffectPtr /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                if (GetTarget())
                    GetTarget()->CastSpell(GetTarget(),138309,true);
            }

            void Register()
            {
                OnEffectPeriodic += AuraEffectPeriodicFn(pool_of_feed_dmg_aura_AuraScript::OnPeriodic, EFFECT_0, SPELL_AURA_PERIODIC_DAMAGE);
                AfterEffectRemove += AuraEffectRemoveFn(pool_of_feed_dmg_aura_AuraScript::OnRemove, EFFECT_0, SPELL_AURA_PERIODIC_DAMAGE, AURA_EFFECT_HANDLE_REAL);

            }
        };

        AuraScript* GetAuraScript() const
        {
            return new pool_of_feed_dmg_aura_AuraScript();
        }
};

// stack of wings remove 133755
class remove_stack_wings : public SpellScriptLoader
{
    public:
        remove_stack_wings() : SpellScriptLoader("remove_stack_wings") { }

        class remove_stack_wings_AuraScript : public AuraScript
        {
            PrepareAuraScript(remove_stack_wings_AuraScript);

            void OnApply(constAuraEffectPtr /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                if (!GetTarget())
                    return;

                if (GetTarget()->HasAura(134339))
                    GetTarget()->GetAura(134339)->ModStackAmount(-1);
            }

            void Register()
            {
                OnEffectApply += AuraEffectApplyFn(remove_stack_wings_AuraScript::OnApply, EFFECT_0, SPELL_AURA_FLY, AURA_EFFECT_HANDLE_REAL_OR_REAPPLY_MASK);

            }
        };

        AuraScript* GetAuraScript() const
        {
            return new remove_stack_wings_AuraScript();
        }
};


// pool of feed(dmg) 68188
class pool_of_feed_dmg : public CreatureScript
{
    public:
        pool_of_feed_dmg() : CreatureScript("pool_of_feed_dmg") { }

        struct pool_of_feed_dmgAI : public ScriptedAI
        {
            pool_of_feed_dmgAI(Creature* creature) : ScriptedAI(creature) 
            {
                me->SetDisplayId(11686);
                me->AddAura(me->GetEntry() == 70216 ? SPELL_FEED_POOL_VISIBLE_HATCHLING : SPELL_FEED_POOL_VISIBLE,me);
            }

            void UpdateAI(const uint32 diff)
            { 
                if (me->GetEntry() != 70216)
                {
                    std::list<Player*> PlayerList;
                    GetPlayerListInGrid(PlayerList, me, 4.0f);
                    for (auto playerTarget : PlayerList)
                        if (!playerTarget->HasAura(SPELL_FEED_POOL_DMG))
                            me->CastSpell(playerTarget,SPELL_FEED_POOL_DMG,true);
                }
            }
        };

        CreatureAI* GetAI(Creature* creature) const
        {
            return new pool_of_feed_dmgAI(creature);
        }
};

// feed of jikun 68178 70130
class npc_jikun_feed : public CreatureScript
{
    public:
        npc_jikun_feed() : CreatureScript("npc_jikun_feed") { }

        struct npc_jikun_feedAI : public ScriptedAI
        {
            npc_jikun_feedAI(Creature* creature) : ScriptedAI(creature) 
            {
               isJump = false;
            }

            bool isJump;

            void UpdateAI(const uint32 diff)
            { 
                std::list<Player*> PlayerList;
                GetPlayerListInGrid(PlayerList, me, 2.0f);
                for (auto player : PlayerList)
                    if (player->HasAura(134339) && !player->HasAura(140741))
                    {
                        player->CastSpell(player,134256,true);
                        player->CastSpell(player,140741,true);
                        me->DespawnOrUnsummon();
                    }

                if (!isJump)
                {
                    if (me->GetEntry() == 70130)
                    {
                        std::list<Creature*> creatureList;
                        GetCreatureListWithEntryInGrid(creatureList, me, 68192, 800.0f);
                        creatureList.remove_if(JadeCore::UnitAuraCheck(true, 134322));

                        if (!creatureList.empty())
                        {
                            if (Creature *hatchling = JadeCore::Containers::SelectRandomContainerElement(creatureList))
                                me->GetMotionMaster()->MoveJump(hatchling->GetPositionX(),hatchling->GetPositionY(),hatchling->GetPositionZ(),15.0f,50.0f);
                            isJump = true;
                        }
                        else
                            me->DespawnOrUnsummon();
                    }
                    else
                    {
                        Position jumpPos;
                        me->GetRandomPoint(IntroMoving[1],45.0f,jumpPos);
                        me->GetMotionMaster()->MoveJump(jumpPos.GetPositionX(),jumpPos.GetPositionY(),6.0f,35.0f,20.0f);
                        isJump = true;
                    }
                }
                else
                {
                    if (me->GetEntry() == 70130)
                    {
                        std::list<Creature*> creatureList;
                        GetCreatureListWithEntryInGrid(creatureList, me, 68192, 1.0f);
                        for (auto hatchLing : creatureList)
                        {
                            DoCast(SPELL_SUMMON_POOL_HATCHLINGS);
                            me->DespawnOrUnsummon();
                            break;
                        }
                    }
                    else
                    {
                        if (me->GetPositionZ() > 5.0f)
                            me->GetMotionMaster()->MoveJump(me->GetPositionX(),me->GetPositionY(),-31.0f,5.0f,17.0f);

                        if (me->GetPositionZ() < -30.0f)
                        {
                            DoCast(SPELL_SUMMON_POOL);
                            me->DespawnOrUnsummon();
                        }
                    }
                }
            }
        };

        CreatureAI* GetAI(Creature* creature) const
        {
            return new npc_jikun_feedAI(creature);
        }
};

// feed summon 137528
class spell_feed_summon : public SpellScriptLoader
{
    public:
        spell_feed_summon() : SpellScriptLoader("spell_feed_summon") { }

        class spell_feed_summon_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_feed_summon_AuraScript);

            void OnTick(constAuraEffectPtr aurEff)
            {
                if (GetCaster())
                    for (uint8 i = 0;i<=3;++i)
                        GetCaster()->SummonCreature(aurEff->GetTickNumber() == 1 ? 70130 : 68178,GetCaster()->GetPositionX(),GetCaster()->GetPositionY(),GetCaster()->GetPositionZ()+6.0f,0.0f,TEMPSUMMON_TIMED_DESPAWN,12000);
            }

            void Register()
            {
                OnEffectPeriodic += AuraEffectPeriodicFn(spell_feed_summon_AuraScript::OnTick, EFFECT_0, SPELL_AURA_PERIODIC_TRIGGER_SPELL);
            }
        };

        AuraScript* GetAuraScript() const
        {
            return new spell_feed_summon_AuraScript();
        }
};

// Ji kun platform teleporter - 70640
class npc_jikun_teleport : public CreatureScript
{
    public:
        npc_jikun_teleport() : CreatureScript("npc_jikun_teleport") { }

        struct npc_jikun_teleportAI : public ScriptedAI
        {
            npc_jikun_teleportAI(Creature* creature) : ScriptedAI(creature) 
            {
                me->SetDisableGravity(true);
                me->SetCanFly(true);
                instance = creature->GetInstanceScript();
            }
            
            InstanceScript* instance;

            void UpdateAI(const uint32 diff)
            {
                std::list<Player*> PlayerList;
                PlayerList.clear();
                if (me->GetPositionZ() > -32.0f)
                    GetPlayerListInGrid(PlayerList, me, 110);
                else
                    GetPlayerListInGrid(PlayerList, me, 130);
                for (auto playerTarget : PlayerList)
                {
                    if (!playerTarget->HasAura(149418) && playerTarget->GetPositionZ() > -8.0f && !playerTarget->IsFlying())
                        if (Creature* jikun = instance->instance->GetCreature(instance->GetData64(NPC_JIKUN)))
                            if (!jikun->isInCombat())
                            {
                                playerTarget->CastSpell(playerTarget,149418,true);
                                playerTarget->JumpTo(24.0f,20.0f);
                            }
                    if (!playerTarget->HasAura(89428) && playerTarget->GetPositionZ() < -108.0f  && !playerTarget->IsFlying())
                    {
                        playerTarget->AddAura(89428,playerTarget);

                        std::list<Creature*> jumpTargets;
                        GetCreatureListWithEntryInGrid(jumpTargets, me, 70640, 1000.0f);
                        for (auto target : jumpTargets)
                        {
                            if (target->GetPositionZ() < -32.0f)
                                continue;
                            float angle = playerTarget->GetAngle(target);
                            float dist = playerTarget->GetDistance(target) - 87.0f;
                            playerTarget->GetMotionMaster()->MoveJump(playerTarget->GetPositionX() + dist * cos(angle),playerTarget->GetPositionY() + dist * sin(angle),-20.0f,15.0f,11.0f);
                        }
                    }
                    if (playerTarget->HasAura(89428) && playerTarget->GetPositionZ() > -21.0f  && !playerTarget->IsFlying())
                    {
                        playerTarget->RemoveAura(89428);
                        std::list<Creature*> jumpTargets;
                        GetCreatureListWithEntryInGrid(jumpTargets, me, 70640, 1000.0f);
                        for (auto target : jumpTargets)
                        {
                            if (target->GetPositionZ() < -32.0f)
                                continue;
                            float angle = playerTarget->GetAngle(target);
                            float dist = playerTarget->GetDistance(target) - 37.0f;
                            playerTarget->GetMotionMaster()->Clear(false);
                            playerTarget->GetMotionMaster()->MoveJump(playerTarget->GetPositionX() + dist * cos(angle),playerTarget->GetPositionY() + dist * sin(angle),-31.0f,30.0f,22.0f);
                        }
                    }
                }
            }
        };

        CreatureAI* GetAI(Creature* creature) const
        {
            return new npc_jikun_teleportAI(creature);
        }
};

// Npc summoning ji kun - 68202
class npc_summon_jikun : public CreatureScript
{
    public:
        npc_summon_jikun() : CreatureScript("npc_summon_jikun") { }

        struct npc_summon_jikunAI : public ScriptedAI
        {
            npc_summon_jikunAI(Creature* creature) : ScriptedAI(creature) 
            {
                instance = creature->GetInstanceScript();
            }

            InstanceScript* instance;

            void Reset()
            {
                if (me->GetPositionZ() > -32.0f)
                    me->SetFlag(UNIT_NPC_FLAGS, UNIT_NPC_FLAG_SPELLCLICK);
                else
                    me->SetFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_NON_ATTACKABLE | UNIT_FLAG_NOT_SELECTABLE);
            }

            void OnSpellClick(Unit* clicker)
            {
                if (instance)
                {
                    if (instance->GetData(DATA_JI_KUN_EVENT) != DONE)
                    {
                        if (Creature* jikun = instance->instance->GetCreature(instance->GetData64(NPC_JIKUN)))
                            jikun->GetAI()->DoAction(1); 

                        me->DespawnOrUnsummon();
                    }
                }
            }
        };

        CreatureAI* GetAI(Creature* creature) const
        {
            return new npc_summon_jikunAI(creature);
        }
};


void AddSC_boss_jikun()
{
    new npc_summon_jikun();
    new npc_jikun_teleport();
    new boss_jikun();
    new spell_infected_talons();
    new spell_caw();
    new egg_mature_jikun();
    new young_hatchling_jikun();
    new npc_juvenile();
    new jikun_beam_target();
    new spell_feed_summon();
    new npc_jikun_feed();
    new pool_of_feed_dmg();
    new pool_of_feed_dmg_aura();
    new remove_stack_wings();
    new spell_hatchling_eated();
}