#include "GameObjectAI.h"
#include "ObjectMgr.h"
#include "ScriptMgr.h"
#include "ScriptedCreature.h"
#include "SpellScript.h"
#include "SpellAuraEffects.h"
#include "SpellAuras.h"
#include "MapManager.h"
#include "Spell.h"
#include "Vehicle.h"
#include "Cell.h"
#include "CellImpl.h"
#include "GridNotifiers.h"
#include "GridNotifiersImpl.h"
#include "CreatureTextMgr.h"
#include "Unit.h"
#include "Player.h"
#include "Creature.h"
#include "InstanceScript.h"
#include "Map.h"
#include "VehicleDefines.h"
#include "SpellInfo.h"

#include "throne_of_thunder.h"

Position Center = { 5895.829f,  4512.626f, -6.276f, 6.242f };

/* DURUMU TODO:
1. Platform gobject, when we in combat platform must be removed and players cant run from boss
2. Maze phase aoe place must be spawned areatriggers 
3. loot
4. lingering gaze problem areatrigger
*/

enum Actions 
{
    ACTION_ROAMING_DIE = 1,
    ACTION_FOG_KILLED  = 2,
    ACTION_SAY_SPELL_1 = 3, // ligtering gaze
    ACTION_SAY_SPELL_2 = 4,
    ACTION_SAY_SPELL_3 = 5,
    ACTION_SAY_SPELL_4 = 6,
    ACTION_MAZE_START = 7,
    ACTION_MAZE_END = 8,
};

enum FogAction
{
    ACTION_FOG_ACTIVATE = 1,
};

enum EyeAction
{
    ACTION_EYE_DRAIN_LIFE = 1,
};

Position YellowEyePositions[8] =
{
    { 5833.541f,  4523.083f, -6.277f, 1.873f },
    { 5859.474f,  4563.136f, -6.277f, 1.873f },
    { 5905.615f,  4573.638f, -6.277f, 1.873f },
    { 5945.436f,  4548.107f, -6.277f, 1.873f },
    { 5955.923f,  4502.487f, -6.277f, 1.873f },
    { 5930.874f,  4463.088f, -6.277f, 1.873f },
    { 5886.584f,  4452.482f, -6.277f, 1.873f },
    { 5845.496f,  4476.399f, -6.277f, 1.873f }
};

enum Spells
{
    SPELL_HARD_STARE = 133765,
    SPELL_GAZE = 134029,
    SPELL_FORCE_OF_WILL = 136932,
    SPELL_FORCE_OF_WILL_KNOCKBACK = 136413,
    SPELL_LINGERING_GAZE_CAST = 138467,
    SPELL_LINGERING_GAZE_MARKER = 134626,
    SPELL_LINGERING_GAZE_DMG = 133792,
    SPELL_BERSERK_KILLALL = 137747,
    SPELL_DRAIN_LIFE_STUN = 137727,
    SPELL_DRAIN_LIFE_CHANNEL = 133795,
    SPELL_DRAIN_LIFE_INCREASE = 133798,
    SPELL_DRAIN_LIFE_HEAL = 133807,
    SPELL_DESINTEGRATION_BEAM = 133776,
    SPELL_MIND_DAGGERS = 139107,

    SPELL_YELLOW_BEAM = 133740,
    SPELL_RED_BEAM = 133734,
    SPELL_BLUE_BEAM = 133672,

    SPELL_FOG_INVISIBLE = 137092,

    SPELL_CAUSTIC_SPIKE = 136154,
    SPELL_ICY_GRASP = 136177,
    SPELL_CRIMSON_BLOOM = 136122,
    SPELL_FLASH_FREEZE = 136124,
};

enum Events
{
    EVENT_HARD_STARE = 1,
    EVENT_GAZE = 2,
    EVENT_FORCE_OF_WILL = 3,
    EVENT_LINGERING_GAZE = 4,
    EVENT_BERSERK = 5,
    EVENT_DRAIN_LIFE = 6,
    EVENT_DRAIN_LIFE_TAR_CHECK = 7,
    EVENT_LIGHT_SPECTRUM_SAY_SUMMON = 8,
    EVENT_LIGHT_SPECTRUM = 9,
    EVENTS_EYES_ORIENTATION = 10,
    EVENT_MAZE_TIME = 11,
    EVENT_MAZE_ORIENTATION = 12,
    EVENT_MAZE_END = 13,
    EVENT_MIND_DAGGERS = 14,
};

enum Yells
{
    SAY_INTRO                                         = 0,
    SAY_AGGRO                                         = 1,
    SAY_DEATH                                         = 2,
    SAY_BERSERK                                       = 3,
    SAY_KILL1                                         = 4,
    SAY_KILL2                                         = 5,
    SAY_SPELL_1                                       = 6,
    SAY_SPELL_2                                       = 7,
    SAY_SPELL_3                                       = 8,
    SAY_SPELL_4                                       = 9,
};

uint64 SelecGUIDtRandomPlayerInRage(Creature*me,float range,bool lingering_gaze = false)
{
    std::list<Player*> PlayerList;
    GetPlayerListInGrid(PlayerList, me, range);
    if (lingering_gaze)
        PlayerList.remove_if(JadeCore::UnitAuraCheck(true, SPELL_LINGERING_GAZE_MARKER));
    if (!PlayerList.empty())
        if (Player *target = JadeCore::Containers::SelectRandomContainerElement(PlayerList))
            return target->GetGUID();

    return 0;
}

uint64 FindDurumuGUID(Creature*me)
{
    std::list<Creature*> durumuList;
    GetCreatureListWithEntryInGrid(durumuList, me, 68036, 200.0f);
    for (auto Durumu : durumuList)
        return Durumu->GetGUID();
    return 0;
}

// Roaming Fog- 68313
class npc_roaming_fog : public CreatureScript
{
    public:
        npc_roaming_fog() : CreatureScript("npc_roaming_fog") { }

        struct npc_roaming_fogAI : public ScriptedAI
        {
            npc_roaming_fogAI(Creature* creature) : ScriptedAI(creature) 
            {
                instance = creature->GetInstanceScript();
            }
            
            uint32 timerMoving;
            InstanceScript* instance;

            void Reset()
            {
                timerMoving = 2000;
            }

            void RandomMove()
            {
                Position position;
                me->GetRandomPoint(Center,40.0f,position);
                me->GetMotionMaster()->MovePoint(0, position);
            }

            void UpdateAI(const uint32 diff)
            { 
                if (!me->isInCombat())
                {
                    if (timerMoving < diff)
                    {
                        RandomMove();
                        timerMoving = urand(3000,6000);
                    }
                    else
                        timerMoving -= diff;
                }

                DoMeleeAttackIfReady();
            }

            void JustDied(Unit* killer)
            {
                if (instance)
                    if (Creature* durumu = ObjectAccessor::GetCreature(*me, FindDurumuGUID(me)))
                        durumu->GetAI()->DoAction(ACTION_ROAMING_DIE);
            }
        };

        CreatureAI* GetAI(Creature* creature) const
        {
            return new npc_roaming_fogAI(creature);
        }
};

// durumu 68036
class boss_durumu : public CreatureScript
{
    public:
        boss_durumu() : CreatureScript("boss_durumu") { }

        struct boss_durumuAI : public BossAI
        {
            boss_durumuAI(Creature* creature) : BossAI(creature, DATA_DURUMU_THE_FORGOTTEN)
            {
                instance = creature->GetInstanceScript();
                RoamingKilled = 0;
            }

            InstanceScript* instance;
            EventMap events;
            uint8 RoamingKilled;
            uint8 FogKilled;

            void Reset()
            {
                _Reset();
                events.Reset();
                summons.DespawnAll();
                SetActivateOrDeactivate();
            }

            void SetFly(bool IsFlying)
            {
                me->SetDisableGravity(IsFlying);
                me->SetCanFly(IsFlying);
            }

            void SetActivateOrDeactivate()
            {
                bool deactivate = RoamingKilled == 3 ? false : true;

                me->SetVisible(deactivate ? false : true);

                if (deactivate) me->SetFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_NON_ATTACKABLE | UNIT_FLAG_NOT_SELECTABLE);
                else me->RemoveFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_NON_ATTACKABLE | UNIT_FLAG_NOT_SELECTABLE);

                me->SetFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_DISABLE_MOVE);
                me->SetReactState(REACT_DEFENSIVE);
                SetFly(true);
                me->SetHomePosition(Center);
                me->GetMotionMaster()->MoveTargetedHome();
            }

            void JustReachedHome()
            {
                _JustReachedHome();
            }

            void JustDied(Unit* killer)
            {
                me->AI()->Talk(SAY_DEATH);
                _JustDied();
            }

            void KilledUnit(Unit* who)
            {
                me->AI()->Talk(urand(0,1) == 0 ? SAY_KILL1 : SAY_KILL2);
            }

            void EnterCombat(Unit* attacker)
            {
                me->AI()->Talk(SAY_AGGRO);
                events.ScheduleEvent(EVENT_HARD_STARE, 5000);
                events.ScheduleEvent(EVENT_GAZE, 2000);
                events.ScheduleEvent(EVENT_BERSERK, 600000);
                events.ScheduleEvent(EVENT_LIGHT_SPECTRUM_SAY_SUMMON, 40000);
                SummonEyes();
            }

            void SendMazeToEyes(bool start)
            {
                std::list<Creature*> eyesList;
                GetCreatureListWithEntryInGrid(eyesList, me, 67875, 200.0f); // mind eye
                GetCreatureListWithEntryInGrid(eyesList, me, 67859, 200.0f); // hungry eye
                GetCreatureListWithEntryInGrid(eyesList, me, 67858, 200.0f); // appraisying eye
                for (auto eye : eyesList)
                    eye->AI()->DoAction(start == true ? ACTION_MAZE_START : ACTION_MAZE_END);
            }

            void SummonBeamsEye()
            {
                if (Creature * eyeDontMover = me->SummonCreature(67856,me->GetPositionX(),me->GetPositionY(),me->GetPositionZ())) // Yellow eye
                    if (Creature * tarMover = me->SummonCreature(67829,YellowEyePositions[0])) // Eyebeam target(mover)
                        eyeDontMover->CastSpell(tarMover,SPELL_YELLOW_BEAM,true);

                // We must use one list for two beams coz not get problem with one player for two beams
                std::list<Player*> PlayerList;
                GetPlayerListInGrid(PlayerList, me, 100.0f);

                if (!PlayerList.empty())
                    if (Creature * eyeDontMover = me->SummonCreature(67854,me->GetPositionX(),me->GetPositionY(),me->GetPositionZ())) // Blue eye
                        if (Player *target = JadeCore::Containers::SelectRandomContainerElement(PlayerList))
                        {
                            eyeDontMover->CastSpell(target,SPELL_BLUE_BEAM,true);
                            PlayerList.remove(target);
                        }
                        
                if (!PlayerList.empty()) // Check again coz we change player list before
                    if (Creature * eyeDontMover = me->SummonCreature(67855,me->GetPositionX(),me->GetPositionY(),me->GetPositionZ())) // Red eye
                        if (Player *target = JadeCore::Containers::SelectRandomContainerElement(PlayerList))
                            eyeDontMover->CastSpell(target,SPELL_RED_BEAM,true);
            }

            void SummonEyes()
            {
                // Minds eye
                me->SummonCreature(67875,me->GetPositionX(),me->GetPositionY(),me->GetPositionZ());
                // Hungry eye
                me->SummonCreature(67859,me->GetPositionX(),me->GetPositionY(),me->GetPositionZ());
                // Appraisying eye
                me->SummonCreature(67858,me->GetPositionX(),me->GetPositionY(),me->GetPositionZ());
            }

            void SummonFog(uint32 entry, uint8 num)
            {
                for (uint8 i=1;i<=num;++i)
                {
                    Position position = Center;
                    while (me->GetDistance(position) <= 7.0f)
                        me->GetRandomPoint(Center,40.0f,position);
                    me->SummonCreature(entry,position);
                }
            }

            void JustSummoned(Creature* summon)
            {
                summons.Summon(summon);
            }

            void UpdateAI(const uint32 diff)
            { 
                if (!UpdateVictim())
                    return;

                events.Update(diff);

                switch (events.ExecuteEvent())
                {
                    case EVENT_HARD_STARE:
                        if (me->getVictim())
                        {
                            DoPlaySoundToSet(me, 35334); // wound
                            DoCast(me->getVictim(),SPELL_HARD_STARE);
                        }
                        events.ScheduleEvent(EVENT_HARD_STARE, 13000);
                        break;
                    case EVENT_GAZE:
                        if (!SelecGUIDtRandomPlayerInRage(me,2.0f))
                            if (!me->HasUnitState(UNIT_STATE_CASTING))
                                DoCast(SPELL_GAZE);
                        events.ScheduleEvent(EVENT_GAZE, 2000);
                        break;
                    case EVENT_BERSERK:
                        events.CancelEvent(EVENT_HARD_STARE);
                        me->AI()->Talk(SAY_BERSERK);
                        DoCast(SPELL_BERSERK_KILLALL);
                        break;
                    case EVENT_LIGHT_SPECTRUM_SAY_SUMMON:
                        FogKilled = 0;
                        me->AI()->Talk(SAY_SPELL_2);
                        SummonFog(69050,3); // 3 red fog
                        SummonFog(69052,1); // 1 blue fog
                        events.ScheduleEvent(EVENT_LIGHT_SPECTRUM, 3000);
                        break;
                    case EVENT_LIGHT_SPECTRUM:
                        SummonBeamsEye();
                        break;
                    case EVENT_MAZE_TIME:
                        Maze(true);
                        events.ScheduleEvent(EVENT_MAZE_ORIENTATION, 5000);
                        events.ScheduleEvent(EVENT_MAZE_END, 47000);
                        events.ScheduleEvent(EVENT_MIND_DAGGERS, 6000);
                        SendMazeToEyes(true);
                        break;
                    case EVENT_MAZE_ORIENTATION:
                        UpdateOrientation();
                        CheckPlayerInFront();
                        events.ScheduleEvent(EVENT_MAZE_ORIENTATION, 200);
                        break;
                    case EVENT_MAZE_END:
                        Maze(false);
                        SendMazeToEyes(false);
                        events.ScheduleEvent(EVENT_LIGHT_SPECTRUM_SAY_SUMMON, 40000);
                        break;
                    case EVENT_MIND_DAGGERS:
                        DoCast(SPELL_MIND_DAGGERS);
                        events.ScheduleEvent(EVENT_MIND_DAGGERS, 6000);
                        break;
                    default:
                        break;
                }

                if (!me->HasUnitState(UNIT_STATE_CASTING))
                    DoMeleeAttackIfReady();
            }

            void Maze(bool start)
            {
                me->SetReactState(start ? REACT_PASSIVE : REACT_AGGRESSIVE);
                start ? events.CancelEvent(EVENT_HARD_STARE) : events.ScheduleEvent(EVENT_HARD_STARE, 2000);
                start ? events.CancelEvent(EVENT_GAZE) : events.ScheduleEvent(EVENT_GAZE, 2000);

                if (!start)
                {
                    std::list<Creature*> beamTarList;
                    GetCreatureListWithEntryInGrid(beamTarList, me, 67829, 200.0f);
                    for (auto Beam : beamTarList)
                        Beam->DespawnOrUnsummon();

                    events.CancelEvent(EVENT_MIND_DAGGERS);
                    events.CancelEvent(EVENT_MAZE_ORIENTATION);

                    me->RemoveAurasDueToSpell(SPELL_DESINTEGRATION_BEAM);
                }
                else
                {
                    me->SetOrientation(6.272f);
                    if (Creature * tarMover = me->SummonCreature(67829,YellowEyePositions[0])) // Eyebeam target(mover)
                        me->CastSpell(tarMover,SPELL_DESINTEGRATION_BEAM,true);
                }
            }

            void UpdateOrientation()
            {
                std::list<Creature*> beamTarList;
                uint64 tarGuid = 0;
                GetCreatureListWithEntryInGrid(beamTarList, me, 67829, 200.0f);
                for (auto Beam : beamTarList)
                    tarGuid = Beam->GetGUID();

                if (Creature* beamTar = ObjectAccessor::GetCreature(*me, tarGuid))
                    me->SetFacingToObject(beamTar);
            }

            void CheckPlayerInFront()
            {
                // In maze time phase all players in front of durumu instant killed
                std::list<Player*> PlayerList;
                GetPlayerListInGrid(PlayerList, me, 200.0f);
                for (auto player : PlayerList)
                    if (me->isInFront(player, M_PI/7))
                        me->Kill(player);
            }

            void EndBeamPhase()
            {
                // Despawn fogs,eyes,yellow eye target
                std::list<Creature*> creatureList;
                GetCreatureListWithEntryInGrid(creatureList, me, 69052, 200.0f); // blue fogs
                GetCreatureListWithEntryInGrid(creatureList, me, 67829, 200.0f); // beam target
                GetCreatureListWithEntryInGrid(creatureList, me, 67854, 200.0f); // red eye
                GetCreatureListWithEntryInGrid(creatureList, me, 67855, 200.0f); // blue eye
                GetCreatureListWithEntryInGrid(creatureList, me, 67856, 200.0f); // yellow eye
                for (auto creature : creatureList)
                    creature->DespawnOrUnsummon();

                // RemoveBeamAuras
                std::list<Player*> PlayerList;
                GetPlayerListInGrid(PlayerList, me, 200.0f);

                for (auto itr : PlayerList)
                {
                    itr->RemoveAurasDueToSpell(SPELL_BLUE_BEAM);
                    itr->RemoveAurasDueToSpell(SPELL_RED_BEAM);
                }
                
                std::list<Creature*> hungryList;
                uint64 guid = 0;
                GetCreatureListWithEntryInGrid(hungryList, me, 67859, 200.0f); // hungry eye
                for (auto eye : hungryList)
                    guid = eye->GetGUID();

                if (Creature* eye = ObjectAccessor::GetCreature(*me, guid)) // find hungry eye to start drain life
                    eye->GetAI()->DoAction(ACTION_EYE_DRAIN_LIFE);

                events.ScheduleEvent(EVENT_MAZE_TIME, 75000);
            }

            void DoAction(const int32 action)
            {
                switch (action)
                {
                    case ACTION_ROAMING_DIE: // When players kill roaming
                        ++RoamingKilled;
                        if (RoamingKilled == 3)
                        {
                            me->AI()->Talk(SAY_INTRO);
                            SetActivateOrDeactivate();
                        }
                        break;
                    case ACTION_FOG_KILLED: // red fog die
                        ++FogKilled;
                        if (FogKilled == 3)
                            EndBeamPhase();
                        break;
                    case ACTION_SAY_SPELL_1: // Lingering gaze
                        me->AI()->Talk(SAY_SPELL_1);
                        break;
                    default:
                        break;
                }
            }
        };

        CreatureAI* GetAI(Creature* creature) const
        {
            return new boss_durumuAI(creature);
        }
};

// durumu fogs
class npc_durumu_fog : public CreatureScript
{
    public:
        npc_durumu_fog() : CreatureScript("npc_durumu_fog") { }

        struct npc_durumu_fogAI : public ScriptedAI
        {
            npc_durumu_fogAI(Creature* creature) : ScriptedAI(creature) 
            {
                Reset();
            }
            
            uint32 timerVisible,timerCastAoe;
            bool activate;

            void Reset()
            {
                me->SetFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_NON_ATTACKABLE | UNIT_FLAG_NOT_SELECTABLE);
                me->SetReactState(REACT_PASSIVE);
                me->SetDisplayId(17286);
                timerVisible = 2000;
                timerCastAoe = 2000;
                activate = false;
            }

            void UpdateAI(const uint32 diff)
            { 
                if (!activate)
                {
                    if (timerVisible < diff)
                    {
                        timerVisible = 8000;
                        if (me->GetEntry() == 69050) // red fog
                            DoCast(SPELL_FOG_INVISIBLE);
                    }
                    else
                        timerVisible -= diff;
                }
                else
                {
                    if (IsMeInFrontOfBeam())
                    {
                        if (!me->HasUnitState(UNIT_STATE_CASTING))
                            DoCast(me->GetEntry() != 69052 ? SPELL_CAUSTIC_SPIKE : SPELL_ICY_GRASP);
                    }
                    else 
                    {
                        if (timerCastAoe < diff)
                        {
                            timerCastAoe = 2000;
                            DoCast(me->GetEntry() != 69052 ? SPELL_CRIMSON_BLOOM : SPELL_FLASH_FREEZE);
                        }
                        else
                            timerCastAoe -= diff;
                    }
                }
            }

            bool IsMeInFrontOfBeam()
            {
                uint32 beamEntry = 0;
                switch (me->GetEntry())
                {
                   case 69052: // blue fog
                       beamEntry = 67854;
                       break;
                   case 69050: // red fog
                       beamEntry = 67855;
                       break;
                   default:
                       break;
                }

                std::list<Creature*> beam;
                GetCreatureListWithEntryInGrid(beam, me, beamEntry, 200.0f);
                for (auto beamEye : beam)
                    if (beamEye->isInFront(me, M_PI/4))
                        return true;

                return false;
            }

            void JustDied(Unit* killer)
            {
                if (me->GetEntry() == 69052) // blue fog when die cast aoe
                    DoCast(SPELL_FLASH_FREEZE);

                if (Creature* durumu = ObjectAccessor::GetCreature(*me, FindDurumuGUID(me)))
                    if (me->GetEntry() == 69050) // red fog
                        durumu->GetAI()->DoAction(ACTION_FOG_KILLED);
            }

            void DoAction(const int32 action)
            {
                switch (action)
                {
                    case ACTION_FOG_ACTIVATE:
                        me->RemoveFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_NON_ATTACKABLE | UNIT_FLAG_NOT_SELECTABLE);
                        me->SetReactState(REACT_AGGRESSIVE);
                        me->SetDisplayId(me->GetNativeDisplayId());
                        activate = true;
                        break;
                    default:
                        break;
                }
            }
        };

        CreatureAI* GetAI(Creature* creature) const
        {
            return new npc_durumu_fogAI(creature);
        }
};

class npc_eyebeam_target : public CreatureScript
{
    public:
        npc_eyebeam_target() : CreatureScript("npc_eyebeam_target") { }

        struct npc_eyebeam_targetAI : public ScriptedAI
        {
            npc_eyebeam_targetAI(Creature* creature) : ScriptedAI(creature) 
            {
                Reset();
            }
            
            bool moveRight,initialize;
            uint32 timeMove,timeInitialize;
            int8 pointNum;

            void Reset()
            {
                me->SetReactState(REACT_PASSIVE);
                me->SetFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_NON_ATTACKABLE | UNIT_FLAG_NOT_SELECTABLE);
                pointNum = 0;
                initialize = false;
                timeInitialize = 4000;
                me->SetDisableGravity(true);
                me->SetCanFly(true);
            }

            void UpdateAI(const uint32 diff)
            { 
                if (!initialize)
                {
                    if (timeInitialize < diff)
                    {
                        urand(0,1) == 0 ? moveRight = true : false;
                        initialize = true;
                        if (me->HasAura(SPELL_DESINTEGRATION_BEAM))
                            timeMove = 4000;
                        else
                        {
                            me->SetSpeed(MOVE_FLIGHT, 0.4f);
                            timeMove = 1000;
                        }
                    }
                    else timeInitialize -= diff;
                }

                if (!initialize)
                    return;

                if (timeMove < diff)
                {
                    pointNum = moveRight == true ? pointNum + 1 : pointNum - 1;

                    if (pointNum > 7)
                        pointNum = 0;
                    if (pointNum < 0)
                        pointNum = 7;

                    if (me->HasAura(SPELL_DESINTEGRATION_BEAM))
                        timeMove = 5000;
                    else
                        timeMove = 15000;
                    me->GetMotionMaster()->MovePoint(0,YellowEyePositions[pointNum]);
                }
                else
                    timeMove -= diff;
            }
        };

        CreatureAI* GetAI(Creature* creature) const
        {
            return new npc_eyebeam_targetAI(creature);
        }
};

class npc_durumu_eye : public CreatureScript
{
    public:
        npc_durumu_eye() : CreatureScript("npc_durumu_eye") { }

        struct npc_durumu_eyeAI : public ScriptedAI
        {
            npc_durumu_eyeAI(Creature* creature) : ScriptedAI(creature) 
            {
                instance = creature->GetInstanceScript();
                me->SetHomePosition(me->GetPositionX(),me->GetPositionY(),me->GetPositionZ(),me->GetOrientation());
            }
            
            uint32 timerCast,timeMove;
            InstanceScript* instance;
            EventMap events;

            void Reset()
            {
                me->SetReactState(REACT_PASSIVE);
                me->SetFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_NON_ATTACKABLE);
                InitializeEvents();
                summons.DespawnAll();
            }

            void JustSummoned(Creature* summon)
            {
                summons.Summon(summon);
            }

            void InitializeEvents()
            {
                switch (me->GetEntry())
                {
                   case 67875: // Mind eye
                       events.ScheduleEvent(EVENT_FORCE_OF_WILL, 32000);
                       break;
                   case 67859: // Hungry eye
                       events.ScheduleEvent(EVENT_DRAIN_LIFE_TAR_CHECK, 1500);
                       break;
                   case 67858: // Appraisying eye
                       events.ScheduleEvent(EVENT_LINGERING_GAZE, 15000);
                       break;
                   case 67856: // Yellow eye
                   case 67854: // Blue eye
                   case 67855: // Red eye
                       events.ScheduleEvent(EVENTS_EYES_ORIENTATION, 1000);
                       me->SetFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_NON_ATTACKABLE | UNIT_FLAG_NOT_SELECTABLE);
                       break;
                   default:
                       break;
                }
                me->SetDisableGravity(true);
                me->SetCanFly(true);
                timeMove = 3000;
                me->SetInCombatWithZone();
            }

            void CheckBetweenPlayerAndDmg()
            {
                std::list<Player*> PlayerList,PlayersInBetween;
                Player * DrainLifeTarget = NULL;
                GetPlayerListInGrid(PlayerList, me, 100.0f);

                for (auto itr : PlayerList)
                    if (itr->HasAura(SPELL_DRAIN_LIFE_STUN))
                        DrainLifeTarget = itr;

                if (DrainLifeTarget != NULL)
                {
                    for (auto itr : PlayerList)
                        if (itr->IsInBetween(me, DrainLifeTarget, 5.0f) && itr->GetGUID() != DrainLifeTarget->GetGUID())
                            PlayersInBetween.push_back(itr);

                    Player * NearstPlayer = DrainLifeTarget;
                    if (NearstPlayer)
                    {
                        for (auto itr : PlayersInBetween)
                            if (itr->GetDistance(me) < NearstPlayer->GetDistance(me))
                                NearstPlayer = itr;

                        DoCast(NearstPlayer,SPELL_DRAIN_LIFE_INCREASE, true);
                    }
                }
            }

            void RandomMove()
            {
                switch (me->GetEntry())
                {
                   // Beam eyes not must move
                   case 67856: // Yellow eye
                   case 67854: // Blue eye
                   case 67855: // Red eye
                       return;
                   default:
                        break;
                }

                Position position;
                me->GetRandomPoint(Center,5.0f,position);
                if (!me->HasUnitState(UNIT_STATE_CASTING))
                    me->GetMotionMaster()->MovePoint(0, position);
            }

            uint64 SelectBeamTarget(float range, bool isBlueOrRedBeam, bool isYellowBeam)
            {
                if (isYellowBeam)
                {
                    std::list<Creature*> beamTarList;
                    GetCreatureListWithEntryInGrid(beamTarList, me, 67829, 200.0f);
                    for (auto Beam : beamTarList)
                        return Beam->GetGUID();
                }
                else
                {
                    std::list<Player*> PlayerList;
                    GetPlayerListInGrid(PlayerList, me, range);

                    for (auto itr : PlayerList)
                        if (itr->HasAura(isBlueOrRedBeam == true ? SPELL_BLUE_BEAM : SPELL_RED_BEAM))
                            return itr->GetGUID();
                }
                return 0;
            }

            void TrySearchFogs()
            {
                std::list<Creature*> fogList;
                GetCreatureListWithEntryInGrid(fogList, me, 69050, 200.0f); // red fogs
                GetCreatureListWithEntryInGrid(fogList, me, 69052, 200.0f); // blue fogs
                for (auto fog : fogList)
                    if (me->isInFront(fog,M_PI/4) && isMyFog(fog->GetEntry()))
                        fog->GetAI()->DoAction(ACTION_FOG_ACTIVATE);
            }

            bool isMyFog(uint32 entry)
            {
                switch (entry)
                {
                    case 69050: // red fog
                        if (me->GetEntry() == 67855) // red beam
                            return true;
                        break;
                    case 69052: // blue fog
                        if (me->GetEntry() == 67854) // blue beam
                            return true;
                        break;
                }
                return false;
            }

            void UpdateAI(const uint32 diff)
            { 
                if (timeMove < diff)
                {
                    RandomMove();
                    timeMove = urand(4000,12000);
                }
                else
                    timeMove -= diff;

                events.Update(diff);

                switch (events.ExecuteEvent())
                {
                    case EVENT_FORCE_OF_WILL:
                        if (Player* target = ObjectAccessor::GetPlayer(*me, SelecGUIDtRandomPlayerInRage(me,100.0f)))
                        {
                            me->SetFacingTo(me->GetAngle(target));
                            DoCast(SPELL_FORCE_OF_WILL);
                        }

                        if (Creature* durumu = ObjectAccessor::GetCreature(*me, FindDurumuGUID(me)))
                            durumu->GetAI()->DoAction(ACTION_SAY_SPELL_1);

                        events.ScheduleEvent(EVENT_FORCE_OF_WILL, 20000);
                        break;
                   case EVENT_LINGERING_GAZE:
                        for (uint8 i=0;i<=1;++i)
                            if (Player* target = ObjectAccessor::GetPlayer(*me, SelecGUIDtRandomPlayerInRage(me,100.0f, true)))
                                DoCast(target,SPELL_LINGERING_GAZE_MARKER);

                        DoCast(SPELL_LINGERING_GAZE_CAST);
                        events.ScheduleEvent(EVENT_LINGERING_GAZE, 45000);
                        break;
                   case EVENT_DRAIN_LIFE:
                       if (Player* target = ObjectAccessor::GetPlayer(*me, SelecGUIDtRandomPlayerInRage(me,100.0f)))
                       {
                           DoCast(target,SPELL_DRAIN_LIFE_STUN);
                           DoCast(target,SPELL_DRAIN_LIFE_CHANNEL);
                       }
                       break;
                   case EVENT_DRAIN_LIFE_TAR_CHECK:
                       CheckBetweenPlayerAndDmg();
                       events.ScheduleEvent(EVENT_DRAIN_LIFE_TAR_CHECK, 1500);
                       break;
                   case EVENTS_EYES_ORIENTATION:
                       if (me->GetEntry() != 67856) // red or blue beam
                       {
                           if (Player* target = ObjectAccessor::GetPlayer(*me, SelectBeamTarget(100.0f, me->GetEntry() == 67854 ? true : false,false)))
                               me->SetFacingToObject(target);
                       }
                       else // yellow beam
                           if (Creature* target = ObjectAccessor::GetCreature(*me, SelectBeamTarget(100.0f, false,true)))
                               me->SetFacingToObject(target);
                       TrySearchFogs();
                       events.ScheduleEvent(EVENTS_EYES_ORIENTATION, 200);
                       break;
                   default:
                        break;
                }

            }

            void DoAction(const int32 action)
            {
                switch (action)
                {
                    case ACTION_EYE_DRAIN_LIFE:
                        events.ScheduleEvent(EVENT_DRAIN_LIFE, 12000);
                        break;
                    case ACTION_MAZE_START:
                        events.CancelEvent(EVENT_LINGERING_GAZE);
                        events.CancelEvent(EVENT_FORCE_OF_WILL);
                        break;
                    case ACTION_MAZE_END:
                        switch (me->GetEntry())
                        {
                           case 67875: // Mind eye
                              events.ScheduleEvent(EVENT_FORCE_OF_WILL, 25000);
                           break;
                           case 67858: // Appraisying eye
                              events.ScheduleEvent(EVENT_LINGERING_GAZE, 19000);
                           break;
                           default:
                              break;
                        }
                        break;
                    default:
                        break;
                }
            }
        };

        CreatureAI* GetAI(Creature* creature) const
        {
            return new npc_durumu_eyeAI(creature);
        }
};

// Arterial Cut 133768
class spell_arterial_cut : public SpellScriptLoader
{
    public:
        spell_arterial_cut() : SpellScriptLoader("spell_arterial_cut") { }

        class spell_arterial_cut_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_arterial_cut_AuraScript);

            void OnTick(constAuraEffectPtr aurEff)
            {
                if (GetTarget())
                    if (GetTarget()->GetHealth() >= GetTarget()->GetMaxHealth())
                        if (GetTarget()->HasAura(GetId()))
                            GetTarget()->RemoveAura(GetId());
            }

            void Register()
            {
                OnEffectPeriodic += AuraEffectPeriodicFn(spell_arterial_cut_AuraScript::OnTick, EFFECT_0, SPELL_AURA_PERIODIC_DAMAGE);
            }
        };

        AuraScript* GetAuraScript() const
        {
            return new spell_arterial_cut_AuraScript();
        }
};

// Drain life dmg calculate
class spell_drain_life_damage : public SpellScriptLoader
{
    public:
        spell_drain_life_damage() : SpellScriptLoader("spell_drain_life_damage") { }

        class spell_drain_life_damage_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_drain_life_damage_SpellScript);

            void CalculateDamage(SpellEffIndex /*effIndex*/)
            { 
                if (!GetCaster() || !GetHitUnit())
                    return;

                int32 amount = GetHitDamage();

                if (GetHitUnit()->HasAura(SPELL_DRAIN_LIFE_INCREASE))
                    amount += (amount * 0.60)*GetHitUnit()->GetAura(SPELL_DRAIN_LIFE_INCREASE)->GetStackAmount();

                int32 bp = amount * 25;
                std::list<Creature*> durumu;
                GetCreatureListWithEntryInGrid(durumu, GetCaster(), 68036, 100.0f);
                for (auto durumuTarget : durumu)
                    durumuTarget->CastCustomSpell(durumuTarget,SPELL_DRAIN_LIFE_HEAL,&bp,NULL,NULL,true);

                SetHitDamage(amount);
            }

            void Register()
            {
                OnEffectHitTarget += SpellEffectFn(spell_drain_life_damage_SpellScript::CalculateDamage, EFFECT_0, SPELL_EFFECT_SCHOOL_DAMAGE);
            }
        };

        SpellScript* GetSpellScript() const
        {
            return new spell_drain_life_damage_SpellScript();
        }
};

// force of will removing 136932
class spell_force_of_will : public SpellScriptLoader
{
    public:
        spell_force_of_will() : SpellScriptLoader("spell_force_of_will") { }

        class spell_force_of_will_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_force_of_will_AuraScript);

            void OnRemove(constAuraEffectPtr /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                if (!GetCaster())
                    return;
                GetCaster()->CastSpell(GetCaster(),SPELL_FORCE_OF_WILL_KNOCKBACK,true);
            }

            void Register()
            {
                AfterEffectRemove += AuraEffectRemoveFn(spell_force_of_will_AuraScript::OnRemove, EFFECT_1, SPELL_AURA_DUMMY, AURA_EFFECT_HANDLE_REAL);
            }
        };

        AuraScript* GetAuraScript() const
        {
            return new spell_force_of_will_AuraScript();
        }
};

// Lingering gaze marker remove 134626
class spell_lingering_gaze : public SpellScriptLoader
{
    public:
        spell_lingering_gaze() : SpellScriptLoader("spell_lingering_gaze") { }

        class spell_lingering_gaze_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_lingering_gaze_AuraScript);

            void OnRemove(constAuraEffectPtr /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                if (!GetTarget() || !GetCaster())
                    return;

                GetCaster()->CastSpell(GetTarget(),SPELL_LINGERING_GAZE_DMG,true);
            }

            void Register()
            {
                AfterEffectRemove += AuraEffectRemoveFn(spell_lingering_gaze_AuraScript::OnRemove, EFFECT_0, SPELL_AURA_DUMMY, AURA_EFFECT_HANDLE_REAL);
            }
        };

        AuraScript* GetAuraScript() const
        {
            return new spell_lingering_gaze_AuraScript();
        }
};

// Beam damage
class spell_beam_aoe_dmg : public SpellScriptLoader
{
    public:
        spell_beam_aoe_dmg() : SpellScriptLoader("spell_beam_aoe_dmg") { }

        class spell_beam_aoe_dmg_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_beam_aoe_dmg_SpellScript);

            void SelectTarget(std::list<WorldObject*>& targets)
            {
                if (!GetCaster())
                    return;

                if (targets.size() == 0)
                {
                    if (GetSpellInfo()->Id == 133732) // redbeam dmg
                        GetCaster()->CastSpell(GetCaster(),133733,true); //aoe dmg to all
                    if (GetSpellInfo()->Id == 133677) // bluebeam dmg
                        GetCaster()->CastSpell(GetCaster(),133678,true); //aoe dmg to all
                    if (GetSpellInfo()->Id == 133738) // yellowbeam dmg
                        GetCaster()->CastSpell(GetCaster(),133739,true); //aoe dmg to all
                }
            }

            void Register()
            {
                OnObjectAreaTargetSelect += SpellObjectAreaTargetSelectFn(spell_beam_aoe_dmg_SpellScript::SelectTarget, EFFECT_0, TARGET_UNIT_CONE_ENEMY_110);
            }
        };

        SpellScript* GetSpellScript() const
        {
            return new spell_beam_aoe_dmg_SpellScript();
        }
};

void AddSC_boss_durumu()
{
    new npc_roaming_fog();
    new boss_durumu();
    new spell_arterial_cut();
    new spell_force_of_will();
    new spell_lingering_gaze();
    new npc_durumu_eye();
    new spell_drain_life_damage();
    new npc_eyebeam_target();
    new npc_durumu_fog();
    new spell_beam_aoe_dmg();
}