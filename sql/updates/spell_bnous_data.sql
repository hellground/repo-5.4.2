
UPDATE `spell_bonus_data` SET `ap_dot_bonus`='0.126',`direct_bonus`='-1',`dot_bonus`='-1',`ap_bonus`='-1' WHERE `entry`='3674';
UPDATE `spell_bonus_data` SET `ap_dot_bonus`='0.391',`direct_bonus`='-1',`dot_bonus`='-1',`ap_bonus`='-1' WHERE `entry`='53301';
UPDATE `spell_bonus_data` SET `ap_dot_bonus`='-1',`direct_bonus`='0.165',`dot_bonus`='-1',`ap_bonus`='-1' WHERE `entry`='73921';
UPDATE `spell_bonus_data` SET `ap_dot_bonus`='0',`direct_bonus`='0.575',`dot_bonus`='0',`ap_bonus`='0' WHERE `entry`='86040';
INSERT INTO `spell_bonus_data` SET `ap_dot_bonus`='0',`direct_bonus`='0',`dot_bonus`='0.5',`ap_bonus`='0';

INSERT INTO `spell_script_names` SET `spell_id`='142723',`ScriptName`='spell_pri_void_shift';


INSERT INTO `spell_linked_spell` SET `spell_trigger`='111758',`spell_effect`='-124433',`type`='2';

DELETE FROM character_pet;
DELETE FROM character_pet_declinedname;
DELETE FROM pet_aura;
DELETE FROM pet_spell;
DELETE FROM pet_spell_cooldown;
UPDATE `characters` SET `currentpetslot`='0', `petslotused`='0';

INSERT INTO `spell_bonus_data` SET `entry`='115748',`ap_dot_bonus`='-1',`direct_bonus`='0.97',`dot_bonus`='-1',`ap_bonus`='-1';
INSERT INTO `spell_bonus_data` SET `entry`='115746',`ap_dot_bonus`='-1',`direct_bonus`='0.97',`dot_bonus`='-1',`ap_bonus`='-1';
INSERT INTO `spell_bonus_data` SET `entry`='115778',`ap_dot_bonus`='-1',`direct_bonus`='0.38',`dot_bonus`='-1',`ap_bonus`='-1';

UPDATE `spell_bonus_data` SET `ap_dot_bonus`='-1',`direct_bonus`='-1',`dot_bonus`='-1',`ap_bonus`='0.168' WHERE `entry`='16827';
UPDATE `spell_bonus_data` SET `ap_dot_bonus`='-1',`direct_bonus`='-1',`dot_bonus`='-1',`ap_bonus`='0.168' WHERE `entry`='17253';



INSERT INTO `spell_linked_spell` SET `spell_trigger`='146555',`spell_effect`='57723',`type`='2',`comment`='drum debuff';

UPDATE `spell_bonus_data` SET `ap_dot_bonus`='1.26',`direct_bonus`='-1',`dot_bonus`='-1',`ap_bonus`='-1' WHERE `entry`='16827';


UPDATE `spell_bonus_data` SET `ap_dot_bonus`='0.8',`direct_bonus`='-1',`dot_bonus`='-1',`ap_bonus`='0.8' WHERE `entry`='53301';

UPDATE `creature_template` SET `modelid1`='39574',`ScriptName`='npc_transcendence_spirit' WHERE `entry`='54569';

INSERT INTO `spell_script_names` SET `spell_id`='119996',`ScriptName`='spell_monk_transcendence_transfer';

INSERT INTO `spell_script_names` SET `spell_id`='146555',`ScriptName`='spell_sha_heroism';


UPDATE `creature_template` SET `modelid1`='45960',`ScriptName`='npc_monk_spirit' WHERE `entry` ='69680';
UPDATE `creature_template` SET `modelid1`='45960',`ScriptName`='npc_monk_spirit' WHERE `entry` ='69791';
UPDATE `creature_template` SET `modelid1`='45960',`ScriptName`='npc_monk_spirit' WHERE `entry` ='69792';

INSERT INTO `spell_script_names` SET `spell_id`='137639',`ScriptName`='spell_monk_storm_earth_and_fire';

UPDATE `spell_bonus_data` SET `ap_dot_bonus`='-1',`direct_bonus`='-1',`dot_bonus`='-1',`ap_bonus`='1' WHERE `entry`='123996';


INSERT INTO `spell_script_names` SET `spell_id`='109263',`ScriptName`='spell_black_ice';
DELETE FROM `spell_proc_event` WHERE `entry`='55440';
UPDATE `spell_proc_event`SET `procFlags`='0' WHERE `entry`='16196';
