UPDATE `creature_template` SET `minlevel`='93',`maxlevel`='93',`faction_A`='14',`faction_H`='14',`mindmg`='57000',`maxdmg`='70000',`attackpower`='120000',`Health_mod`='560',`mechanic_immune_mask`='769638399',`ScriptName`='boss_jikun' WHERE `entry`='69712';

INSERT INTO `creature` SET `guid`='998113',`id`='70640',`map`='1098',`spawnMask`='8',`phaseMask`='1',`position_x`='6147',
`position_y`='4318',`position_z`='-31,1',`orientation`='1.08972',`spawntimesecs`='300',`curhealth`='100',`unit_flags`='33554432',`unit_flags2`='2048';

INSERT INTO `creature` SET `guid`='998115',`id`='70640',`map`='1098',`spawnMask`='8',`phaseMask`='1',`position_x`='6147',`position_y`='4318',
`position_z`='-131,1',`orientation`='1.08972',`spawntimesecs`='300',`curhealth`='100',`unit_flags`='33554432',`unit_flags2`='2048';

INSERT INTO `creature` SET `guid`='998842',`id`='68202',`map`='1098',`spawnMask`='8',`phaseMask`='1',`position_x`='6147',`position_y`='4318',
`position_z`='-31,1',`orientation`='1.08972',`spawntimesecs`='300',`curhealth`='100';

INSERT INTO `creature` SET `guid`='998640',`id`='69712',`map`='1098',`spawnMask`='8',`phaseMask`='1',`position_x`='6192,68',`position_y`='4268,42',
`position_z`='-70,5738',`orientation`='1.08972',`spawntimesecs`='300',`curhealth`='244236720';

UPDATE `creature_template` SET `ScriptName`='npc_jikun_teleport' WHERE `entry`='70640';
UPDATE `creature_template` SET `ScriptName`='npc_summon_jikun' WHERE `entry`='68202';
UPDATE `creature_template` SET `health_mod`='4500',`ScriptName`='egg_mature_jikun' WHERE `entry`='69628';
UPDATE `creature_template` SET `health_mod`='4300',`ScriptName`='young_hatchling_jikun' WHERE `entry`='68192';
UPDATE `creature_template` SET `health_mod`='4300',`ScriptName`='npc_juvenile' WHERE `entry`='70095';
UPDATE `creature_template` SET `ScriptName`='jikun_beam_target' WHERE `entry`='68208';
UPDATE `creature_template` SET `modelid2`='0',`modelid1`='48142' WHERE `entry`='68178';
UPDATE `creature_template` SET `modelid2`='0',`modelid1`='48210' WHERE `entry`='70130';
UPDATE `creature_template` SET `ScriptName`='npc_jikun_feed' WHERE `entry`='68178';
UPDATE `creature_template` SET `ScriptName`='npc_jikun_feed' WHERE `entry`='70130';
UPDATE `creature_template` SET `ScriptName`='pool_of_feed_dmg' WHERE `entry`='68188';
UPDATE `creature_template` SET `ScriptName`='pool_of_feed_dmg' WHERE `entry`='70216';


INSERT INTO `spell_script_names` SET `spell_id`='140094',`ScriptName`='spell_infected_talons';

INSERT INTO `spell_script_names` SET `spell_id`='138923',`ScriptName`='spell_caw';

INSERT INTO `spell_script_names` SET `spell_id`='137528',`ScriptName`='spell_feed_summon';
INSERT INTO `spell_script_names` SET `spell_id`='138319',`ScriptName`='pool_of_feed_dmg_aura';
INSERT INTO `spell_script_names` SET `spell_id`='134256',`ScriptName`='pool_of_feed_dmg_aura';
INSERT INTO `spell_script_names` SET `spell_id`='133755',`ScriptName`='remove_stack_wings';
INSERT INTO `spell_script_names` SET `spell_id`='134321',`ScriptName`='spell_hatchling_eated';