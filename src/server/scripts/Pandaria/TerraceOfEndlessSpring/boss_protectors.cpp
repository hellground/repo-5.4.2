#include "GameObjectAI.h"
#include "ScriptMgr.h"
#include "ScriptedCreature.h"
#include "terrace_of_endless_spring.h"

class boss_protector_kaolan : public CreatureScript
{
    public:
        boss_protector_kaolan() : CreatureScript("boss_protector_kaolan") { }

        struct boss_protector_kaolanAI : public BossAI
        {
            boss_protector_kaolanAI(Creature* creature) : BossAI(creature, DATA_PROTECTORS)
            {
                pInstance = creature->GetInstanceScript();
            }

            InstanceScript* pInstance;
            EventMap events;

            uint8 attacksCounter;
            uint8 terrorCounter;

            void Reset()
            {
                _Reset();
 
                summons.DespawnAll();

                events.Reset();
            }

            void JustReachedHome()
            {

                _JustReachedHome();

                if (pInstance)
                    pInstance->SetBossState(DATA_PROTECTORS, FAIL);
            }

            void EnterCombat(Unit* attacker)
            {
                if (pInstance)
                {
                    pInstance->SetBossState(DATA_PROTECTORS, IN_PROGRESS);
                    pInstance->SendEncounterUnit(ENCOUNTER_FRAME_ENGAGE, me);
                    DoZoneInCombat();
//                    Talk(TALK_AGGRO);
                }
            }

            void JustDied(Unit* killer)
            {
                if (pInstance)
                {
                    summons.DespawnAll();

                    pInstance->SendEncounterUnit(ENCOUNTER_FRAME_DISENGAGE, me);
                    _JustDied();
                }
            }

            void JustSummoned(Creature* summon)
            {

                summons.Summon(summon);
            }

            void SummonedCreatureDespawn(Creature* summon)
            {

                summons.Despawn(summon);
            }

            void KilledUnit(Unit* who)
            {
                if (who->GetTypeId() == TYPEID_PLAYER)
                {
///                        Talk(TALK_SLAY_HEROIC);
//                        Talk(TALK_SLAY);
                }
            }

            void DoAction(const int32 action)
            {

            }

            void DamageDealt(Unit* /*victim*/, uint32& /*damage*/, DamageEffectType damageType)
            {

            }

            void UpdateAI(const uint32 diff)
            {
                if (me->HasUnitState(UNIT_STATE_CASTING))
                    return;



                DoMeleeAttackIfReady();
            }
        };


};


void AddSC_boss_protectors()
{

}